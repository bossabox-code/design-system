# Icons

## Steps to update icons with new icons from figma

1. The `svg` icons folder should be updated with the svg icons from: https://www.figma.com/file/492S02xnwQ0TYQPSaRTsS4/Design-System?node-id=2%3A5

1. Run `npm run generate-icons` to generate icons into `icons` folder.
