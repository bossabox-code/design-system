const fs = require('fs');
const slugify = require('slugify');
const camelcase = require('camelcase');

slugify.extend({ _: '-' });

/**
 * This will be deleted after we update all projects using the old icons to the new icons format.
 */
const tempPrefixToPreventAppFromBreakChanges = 'b-new-';
const tempPrefixToPreventAppFromBreakChangesExport = 'BNew';

const iconComponentTemplate = ({ svg, name }) => `
<template>
	<i>
		${svg}
	</i>
</template>

<script>
export default {
	name: \`${tempPrefixToPreventAppFromBreakChanges}b-${name}\`,
	components: {},
	computed: {},
	props: {
		spin: {
			type: Boolean,
			default: false
		}
	},
	mounted() {
		if(this.spin){
			this.$nextTick(() => this.$el.classList.add('spin'));
		}
	}
};
</script>

<style scoped lang="scss">
i {
	display: inline-block;
	vertical-align: middle;

	svg {
		width: 1em;
		height: auto;
		display: block;
	}

	*:not([fill='none']) {
		fill: currentColor;
	}
}

path.a {
	fill: none;
}

.spin {
	animation: rotation 2s infinite linear;
}

@keyframes rotation {
	from {
		transform: rotate(0deg);
	}
	to {
		transform: rotate(359deg);
	}
}
</style>

`;

const iconDocsTemplate = ({ componentName }) =>
	`
import { Meta, Story, Canvas } from '@storybook/addon-docs';

import { ${componentName} } from '../../new_icons/index.js';

<Meta title="Componentes/Icon/${componentName}" component={${componentName}} />

# Icon - ${componentName}

export const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ${componentName} },
  template: '<${componentName} v-bind="$props"/>',
});

Padrão do componente

<Canvas>
	<Story name="Default" args={{ style: { fontSize: '60px' } }}>
			{Template.bind({})}
	</Story>
</Canvas>
`;

// remove style and breaklines
const formatSvg = ({ rawSvg }) => {
	let formatedSvg = rawSvg;

	formatedSvg = formatedSvg
		.replace(/(\r\n|\n|\r)/gm, '')
		.replace(/\sstyle="[^\s]*"/g, ' ')
		.replace(/\sxmlns="[^\s]*"/g, ' ')
		.replace(/"/g, "'")
		.replace(/>\s+</g, '><')
		.replace(/\s>/, '>');

	return formatedSvg;
};

const generateIcons = async () => {
	const iconsFolder = './components/new_icons/svg';
	const iconsComponentFolder = './components/new_icons/icons';
	const iconsComponentDocsFolder = './components/stories/icons';
	const svgFiles = await fs.readdirSync(iconsFolder);
	const indexFile = await fs.readFileSync('./components/new_icons/index.js', 'utf-8');
	const [indexFileContent] = indexFile.split('// Auto Generated Icons');
	let indexExportIconsContent = '';
	let fileName = '';
	let docFileName = '';
	let content = '';
	const errors = [];

	const readFile = resolve => {
		content = fs.readFileSync(`${iconsFolder}/${fileName}`, 'utf-8');
		resolve();
	};

	const saveIconsDataIntoAFile = () => {
		const svg = formatSvg({ rawSvg: content });

		fileName = `icon_${fileName.replace('.svg', '')}`;

		const componentDocName = camelcase(fileName, { pascalCase: true });
		const componentName = slugify(fileName);

		indexExportIconsContent += `\nexport { default as ${tempPrefixToPreventAppFromBreakChangesExport}B${componentDocName} } from './icons/${componentDocName}';`;

		try {
			fs.writeFileSync(
				`${iconsComponentFolder}/${`${componentDocName}.vue`}`,
				`${iconComponentTemplate({
					svg,
					name: componentName
				})}\n`
			);

			console.log(`Component: ${fileName}`);
		} catch (error) {
			errors.push(`Component${fileName}`);
			throw error;
		}
	};

	const saveIconsDocIntoAFile = () => {
		docFileName = fileName.replace('.svg', '');

		const componentDocName = camelcase(docFileName, { pascalCase: true });

		try {
			fs.writeFileSync(
				`${iconsComponentDocsFolder}/${`${componentDocName}.stories.mdx`}`,
				`${iconDocsTemplate({
					componentName: `${tempPrefixToPreventAppFromBreakChangesExport}B${componentDocName}`
				})}\n`
			);

			console.log(`Docs: ${docFileName}`);
		} catch (error) {
			errors.push(`Docs${docFileName}`);
			throw error;
		}
	};

	const updateIndexWithIconsExport = () => {
		fs.writeFileSync(
			`./components/new_icons/index.js`,
			`${indexFileContent}// Auto Generated Icons\n${indexExportIconsContent}\n`
		);
	};

	console.log('\n--------------------------------- \n');

	for (let index = 0; index < svgFiles.length; index += 1) {
		fileName = svgFiles[index];

		if (fileName.indexOf('.svg') !== -1) {
			await new Promise(resolve => readFile(resolve))
				.then(saveIconsDataIntoAFile())
				.then(saveIconsDocIntoAFile())
				.then(updateIndexWithIconsExport());
		}
	}

	if (errors.length) {
		console.log('\n---------------------------------');
		console.log('Unable to create icons 😔');
		console.log('---------------------------------');
		console.log('Icons not generated error: ', errors);
		return;
	}

	console.log('\n---------------------------------');
	console.log('Icons Generated 😄');
	console.log('---------------------------------');
};

generateIcons();
