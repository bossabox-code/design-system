# BossaBox - Design System
Biblioteca de componentes visuais da BossaBox. 

## Instalação
Para instalar o pacote no projeto, basta executar:
```
yarn add @bossabox-code/design-system
``` 

Para que todos os componentes funcionem perfeitamente, é necessário criar um arquivo chamado `dlsConfiguration.js` com o código abaixo e importar no arquivo `main.js`:
```
import VCalendar from 'v-calendar';
import Vue from 'vue';
import { VueHammer } from 'vue2-hammer';

Vue.use(VueHammer);
Vue.use(VCalendar);

const req = require.context('@bossabox-code/design-system/icons/', true, /\.(svg)$/i);
req.keys().map(key => {
	const name = key.substring(key.indexOf('/') + 1, key.lastIndexOf('.'));
	return Vue.component(name, req(key).default);
});

```
## Instalar as dependências de um projeto que tem o design-system pela primeira vez
Antes de instalar as dependências de um projeto que tem o design-system, é importante lembrar de executar o seguinte comando: 

```
yarn config set @bossabox-code:registry https://gitlab.com/api/v4/packages/npm/

yarn install
```
## Semantic Releases
Utilizamos a biblioteca [semantic-release](https://github.com/semantic-release/semantic-release) para automatizar o versionamento e a publicação de novas versões. Atualmente, as tags que estão habilitadas para fazer release são: `feat, fix, perf, chore, ci, docs, refactor`

## Merge Requests
As merge requests abertas devem seguir o seguinte padrão na descrição:
- Nome da história
- Link da história no clickup (se houver)
- Link do figma (se houver)
- Descrição dos arquivos que foram alterados
- Resultados: screenshots, gifs, vídeos

## Testando com alterações locais
Para ser possível testar o pacote realizando alterações locais, utilizamos `yarn link`:
1. Acesse a pasta do dls no seu computador e execute o comando `yarn link --link-folder ~/.links`;
2. Acesse a pasta do projeto que utiliza o dls e execute o comando `yarn link --link-folder ~/.links "@bossabox-code/design-system"`;

Para finalizar a ligação, basta seguir os seguintes passados:
1. Acesse a pasta do projeto que utiliza o dls e execute o comando `yarn unlink --link-folder ~/.links "@bossabox-code/design-system"`;
2. Acesse a pasta do dls no seu computador e execute o comando `yarn unlink --link-folder ~/.links`;
3. Volte na pasta do projeto que utiliza o dls e execute `yarn install --force`;


## Documentação - Storybook
A documentação pode ser acessada por esse link: http://dls.bossabox.com/

Caso queira rodar localmente, basta executar o seguinte comando: `yarn storybook`.
