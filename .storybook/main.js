module.exports = {
	"stories": [
	  "../**/*.stories.mdx",
	  "../**/**/*.stories.@(js|jsx|ts|tsx)"
	],
	"addons": [
	  "@storybook/addon-links",
	  "@storybook/addon-essentials",
	  '@storybook/preset-scss',
	  '@storybook/addon-docs',
	  '@storybook/addon-controls',
	],
};
  
  
  
  