import '../css/_variables.scss';
import '../css/elevation.scss';
import '../css/grid.scss';
import '../css/position.scss';
import '../css/spacing.scss';
import '../css/styling.scss';
import '../css/typography.scss';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}